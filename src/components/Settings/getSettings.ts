import {makeRequest} from '../../utils/makeRequest';


/**
 * Get or Create server settings
 * @param {number} id
 * @return {Promise<any>}
 */
export const getSettings = (id: number | string | null): Promise<any> => {
  return new Promise((resolve, reject) => {
    makeRequest({
      method: 'POST',
      url: `${process.env.API_URL}/settings/${id}`,
    }).then((res) => {
      resolve(res);
    }).catch((err) => console.log('err', err));
  });
};
