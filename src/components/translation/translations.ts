import emojis from '../../utils/emoji';
/**
 *
 * @param {string} keyTranslation
 * @param {object} options
 * @return {string}
 */
const t = (keyTranslation: string, options: {
  lang: string,
  variables?: object,
}): string => {
  try {
    if (!options.lang) return keyTranslation;
    const lang: object = require(`../../translations/${options.lang}.json`);
    let str = lang[keyTranslation];
    if (!str) return keyTranslation;

    const emoji: object = {
      'success': emojis.success,
      'error': emojis.error,
      'warning': emojis.warning,
      'loading': emojis.loading,
      'information': emojis.information,
      'denied': emojis.denied,
    };

    for (const k of Object.keys(emoji)) {
      const re = new RegExp(`{${k}}`, 'gi');
      if (re) str = str.replace(re, emoji[k]);
    }

    if (options.variables) {
      for (const i of Object.keys(options.variables)) {
        const re = new RegExp(`{${i}}`, 'gi');
        if (re) str = str.replace(re, options.variables[i]);
      }
    }

    return str;
  } catch (e) {
    switch (e.code) {
      case 'MODULE_NOT_FOUND':
        return keyTranslation;
    }
  }
};

export default t;
