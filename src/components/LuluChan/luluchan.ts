import {Client, ClientOptions} from 'discord.js';
import * as log from '../../utils/log';
import {loadFiles} from '../../utils/handler/loadFiles';
import {getProjectDir, io} from '../../app';
import {loadCommands} from '../../utils/handler/loadCommands';

interface ClientInterface extends ClientOptions {
  development: boolean;
  token: string;
}

class LuluChan extends Client {
  public development: boolean;
  public token: string;

  constructor(config: ClientInterface) {
    super(config);

    // Define the variable
    this.development = config.development;
    this.token = config.token;

    // Add message if development is enable
    if (config.development) log.danger('Dev Mode is Enable');

    // Load all command
    loadCommands().then(async () => {
      // LOAD Event
      log.info('Load Event ...');
      await loadFiles({
        dirPath: getProjectDir + '/events/',
        client: this,
        handleType: 'event',
      });

      log.info('Load Socket Event ...');
      io.on('connection', async (socket) => {
        await loadFiles({
          dirPath: getProjectDir + '/socket/',
          socket,
          client: this,
          handleType: 'socketEvent',
        });
      });

      log.info('Load Command ...');
      await loadFiles({
        dirPath: getProjectDir + '/commands/',
        client: this,
        handleType: 'command',
      });

      // Start the BOT
      await this.login(this.token);
    });
  }
}

export default LuluChan;
