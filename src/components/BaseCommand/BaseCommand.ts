import {Message} from 'discord.js';
import {BaseCommandParams} from './BaseCommandParams';

export type CategoryInterface = 'moderator'|'image'|'emote'|'reaction'|'hentai'|'porn'|'misc'|'setting'|'admin'|'nsfw'|'music'

export abstract class BaseCommand extends BaseCommandParams {
  abstract alias: string[];
  abstract allowDM: boolean;
  abstract category: CategoryInterface;
  abstract cooldown: number;
  abstract description: string;
  abstract disable: boolean;
  abstract example: string;
  abstract onlyDev: boolean;

  abstract execute = (): Promise<Message> => {
    return;
  }
}
