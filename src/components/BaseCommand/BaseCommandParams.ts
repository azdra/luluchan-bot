import LuluChan from '../LuluChan/luluchan';
import {
  Guild,
  GuildMember,
  HexColorString,
  Message,
  MessageEmbed,
  MessageEmbedOptions,
  TextBasedChannels,
  User,
} from 'discord.js';
import {Setting} from '../../types/Setting';
import {CommandParams} from '../../types/CommandParams';
import t from '../translation/translations';

export class BaseCommandParams implements CommandParams {
  readonly client: LuluChan;
  readonly message: Message;
  readonly prefix: string;
  readonly guild: Guild;
  readonly channel: TextBasedChannels;
  readonly author: User;
  readonly settings: Setting;
  readonly args: string[];
  readonly command: string;
  readonly member: GuildMember;

  protected constructor({client, message, prefix, settings, command}: CommandParams) {
    if (!client) return;
    this.client = client;
    this.message = message;
    this.prefix = prefix;
    this.settings = settings;

    this.guild = this.message.guild;
    this.channel = this.message.channel;
    this.author = this.message.author;
    this.command = command;
    this.args = this.getArgs() ?? [];

    this.member = this.getMember();
  }

  private getArgs = () => {
    const args = this.message.content.slice(this.prefix.length).trim().split(/ +/g);
    args.shift();
    return args;
  }

  private getMember = (): GuildMember => {
    const _m = this.message.mentions.members.first() || (this.args[0] ? this.guild.members.cache.get(this.args[0]) : false);
    if (!_m) return this.guild.members.cache.get(this.author.id);
    return _m;
  }

  isMe = () => {
    return this.author.id === this.member.id;
  }

  /**
   * Create Embed Message
   * @param {MessageEmbedOptions} data
   * @return {MessageEmbed}
   */
  embed = (data: MessageEmbedOptions): MessageEmbed => {
    return new MessageEmbed(data)
        .setColor(data.color ?? this.settings.color as HexColorString);
  };

  /**
   * @param {MessageEmbedOptions} embedOptions
   * @return {Promise<Message>}
   */
  messageEmbed = (embedOptions: MessageEmbedOptions): Promise<Message> => {
    const embed = this.embed(embedOptions);
    return this.channel.send({embeds: [embed]});
  }

  /**
   * @param {string} content
   * @param {{delete: number}} options
   * @param {number} options.delete In second
   * @return {Promise<Message>}
   */
  reply = async (content: string, options?: { delete: number }): Promise<Message> => {
    const message = await this.message.reply(content);
    if (message && options) {
      if (options.delete) setTimeout(() => message.delete(), options.delete);
    }
    return message;
  }

  /**
   * Translate the key of phrase
   * @param {string} key
   * @param {object} variables
   * @return {string}
   */
  translation = (key: string, variables?: object) => t(key, {
    lang: this.settings.lang || 'en',
    variables: variables,
  });

  /**
   * @param {string} timestamp
   * @return {string}
   */
  parseUnixTimestamp = (timestamp: number): string => {
    const date = new Date(timestamp * 1000);
    const getDay = date.getDate();
    const getMonth = date.getMonth()+1;

    return `${getMonth}/${getDay}/${date.getFullYear()}`;
  }
}
