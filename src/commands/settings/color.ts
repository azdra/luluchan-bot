import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {HexColorString, Message} from 'discord.js';
import {CommandParams} from '../../types/CommandParams';
import color from '../../utils/color';
import {makeRequest} from '../../utils/makeRequest';
import {cacheSettings} from '../../app';

export const hexColorRegExp = /^#([A-Fa-f0-9]{3}([A-Fa-f0-9]{3})?)$/;

export default class SettingsColorCommands extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['color'];
    this.allowDM = false;
    this.category = 'setting';
    this.cooldown = 10;
    this.description = 'jsp';
    this.disable = false;
    this.example = 'jsp';
    this.onlyDev = false;
  }

  private saveColor = (color: HexColorString) => makeRequest({
    method: 'PUT',
    url: `${process.env.API_URL}/settings/${this.guild.id}`,
    data: {color},
  }).then(() => {
    // Update cache
    cacheSettings.get(this.guild.id).color = color;

    // Send success message
    return this.messageEmbed({
      color,
      description: this.translation('COLOR_CHANGED', {
        color,
      }),
    });
  });

  execute = async (): Promise<Message> => {
    // Check right permissions
    if (!this.message.member.permissions.has('MANAGE_GUILD')) {
      return this.messageEmbed({
        color: color.danger,
        description: this.translation('BAD_PERMISSION'),
      });
    }

    // Check first argument is given
    if (!this.args[0]) {
      return this.messageEmbed({
        color: color.warning,
        description: this.translation('COLOR_MISSING', {
          defaultColor: process.env.DEFAULT_COLOR,
        }),
      });
    }

    if (this.args[0].match(hexColorRegExp)) {
      await this.saveColor(this.args[0] as HexColorString);
    } else if (this.args[0].toLowerCase() === 'default') {
      await this.saveColor(process.env.DEFAULT_COLOR as HexColorString);
    } else {
      return this.messageEmbed({
        color: color.warning,
        description: this.translation('COLOR_MISSING', {
          defaultColor: process.env.DEFAULT_COLOR,
        }),
      });
    }
  }
}
