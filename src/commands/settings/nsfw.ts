import {Message} from 'discord.js';
import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import color from '../../utils/color';

export default class SettingsNsfwCommands extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['nsfw'];
    this.allowDM = true;
    this.category = 'setting';
    this.cooldown = 2;
    this.description = 'utils';
    this.disable = false;
    this.example = 'jsp';
    this.onlyDev = false;
  }

  execute = (): Promise<Message> => {
    if (!this.message.member.permissions.has('MANAGE_GUILD')) {
      return this.messageEmbed({
        color: color.danger,
        description: this.translation('BAD_PERMISSION'),
      });
    }

    if (this.channel.type === 'GUILD_TEXT') {
      if (this.channel.nsfw) {
        return this.channel.setNSFW(false).then(() => this.messageEmbed({
          color: color.danger,
          description: this.translation('NSFW_DISABLE'),
        }));
      } else {
        return this.channel.setNSFW(true).then(() => this.messageEmbed({
          color: color.success,
          description: this.translation('NSFW_ENABLED'),
        }));
      }
    }
  };
}
