import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {Message} from 'discord.js';
import {CommandParams} from '../../types/CommandParams';
import color from '../../utils/color';
import {makeRequest} from '../../utils/makeRequest';
import {cacheSettings} from '../../app';

export default class SettingsPrefixCommands extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['prefix'];
    this.allowDM = false;
    this.category = 'setting';
    this.cooldown = 10;
    this.description = 'jsp';
    this.disable = false;
    this.example = 'jsp';
    this.onlyDev = false;
  }

  private savePrefix = (prefix: string) => makeRequest({
    method: 'PUT',
    url: `${process.env.API_URL}/settings/${this.guild.id}`,
    data: {prefix},
  }).then(() => {
    // Update cache
    cacheSettings.get(this.guild.id).prefix = prefix;

    // Send success message
    return this.messageEmbed({
      color: color.success,
      description: this.translation('PREFIX_CHANGED'),
    });
  })

  execute = (): Promise<Message> => {
    // Check right permissions
    if (!this.message.member.permissions.has('MANAGE_GUILD')) {
      return this.messageEmbed({
        color: color.danger,
        description: this.translation('BAD_PERMISSION'),
      });
    }

    // Check first argument is given
    if (!this.args[0]) {
      return this.messageEmbed({
        color: color.warning,
        description: this.translation('PREFIX_MISSING'),
      });
    }

    if (this.prefix.length > parseInt(process.env.PREFIX_MAX_LENGTH)) {
      return this.messageEmbed({
        color: color.warning,
        description: this.translation('PREFIX_MAX_LENGTH'),
        footer: {
          text: this.translation('PREFIX_LENGTH', {
            maxLength: process.env.PREFIX_MAX_LENGTH,
          }),
        },
      });
    }

    return this.savePrefix(this.args[0]);
  }
}
