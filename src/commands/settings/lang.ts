import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import {Message} from 'discord.js';
import color from '../../utils/color';
import {makeRequest} from '../../utils/makeRequest';
import {cacheSettings} from '../../app';
import t from '../../components/translation/translations';

type LanguageInfo = {
  lang: string;
  flag: string;
  id: string;
}

export const languagesList: {
  [lang: string]: LanguageInfo
} = {
  'en': {
    'id': 'en',
    'lang': 'English',
    'flag': ':flag_gb:',
  },
  'fr': {
    'id': 'fr',
    'lang': 'French (Français)',
    'flag': ':flag_fr:',
  },
};

export default class SettingsLangCommands extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['lang'];
    this.allowDM = false;
    this.category = 'setting';
    this.cooldown = 10;
    this.description = 'jsp';
    this.disable = false;
    this.example = 'jsp';
    this.onlyDev = false;
  }

  /**
   * @param {LanguageInfo} language
   * @return {Promise<Message>}
   */
  private saveLanguage = (language: LanguageInfo): Promise<Message> => makeRequest({
    method: 'PUT',
    url: `${process.env.API_URL}/settings/${this.guild.id}`,
    data: {lang: language.lang},
  }).then(() => {
    // Update cache
    cacheSettings.get(this.guild.id).lang = language.lang;

    // Send success message
    return this.messageEmbed({
      color: color.success,
      description: t('LANG_CHANGED', {lang: language.lang}),
    });
  })

  /**
   * @return {Promise<Message>}
   */
  private sendList = (): Promise<Message> => {
    let list = '';

    for (const key of Object.keys(languagesList)) {
      if (languagesList.hasOwnProperty(key)) {
        const {id, lang, flag} = languagesList[key];
        list += `${flag} • \`\`${id}\`\` • ${lang}\n`;
      }
    }

    return this.messageEmbed({
      title: this.translation('LANG_LIST_AVAILABLE'),
      color: color.info,
      description: list,
      footer: {
        text: this.translation('LANG_LIST_FOOTER'),
      },
    });
  }

  execute = (): Promise<Message> => {
    // Check right permissions
    if (!this.message.member.permissions.has('MANAGE_CHANNELS')) {
      return this.messageEmbed({
        color: color.danger,
        description: this.translation('BAD_PERMISSION'),
      });
    }

    // Send list if the first arg do not given
    if (!this.args[0]) return this.sendList();
    const firstArg = this.args[0].toLowerCase();

    if (firstArg === 'list') {
      return this.sendList();
    } else if (languagesList[firstArg]) {
      return this.saveLanguage(languagesList[firstArg]);
    } else {
      return this.sendList();
    }
  }
}
