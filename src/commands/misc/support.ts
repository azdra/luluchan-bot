import {Message} from 'discord.js';
import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';

export default class SupportCommand extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  private readonly invite: string;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['support'];
    this.allowDM = true;
    this.category = 'misc';
    this.cooldown = 0;
    this.description = 'utils';
    this.disable = false;
    this.example = '{prefix} support';
    this.onlyDev = false;
    this.invite = process.env.INVITE_LINK;
  }

  execute = (): Promise<Message> => {
    return this.messageEmbed({
      description: this.translation('SUPPORT', {
        inviteLink: process.env.INVITE_LINK,
      }),
      footer: {
        text: this.invite ?? this.translation('INVALID_LINK'),
      },
    });
  }
}
