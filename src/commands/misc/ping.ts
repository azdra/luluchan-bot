import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import {Message} from 'discord.js';

export default class PingCommand extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['ping'];
    this.allowDM = true;
    this.category = 'misc';
    this.cooldown = 5;
    this.description = 'Recovers latency';
    this.disable = false;
    this.example = '{prefix} ping';
    this.onlyDev = false;
  }

  execute = async (): Promise<Message> => {
    const loading = await this.reply(this.translation('PING_LOADING'));

    const clientPing = loading.createdTimestamp - this.message.createdTimestamp;
    const apiPing = this.client.ws.ping;

    const msg = this.translation('PING_RESULT', {
      bot: clientPing.toString(),
      discord: apiPing.toString(),
    });

    const embed = this.embed({
      title: '🏓 Pong!',
      description: msg,
      timestamp: new Date(),
    });

    return loading.edit({
      embeds: [embed],
      content: ' ',
    });
  }
}
