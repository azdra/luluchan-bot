import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import {Message} from 'discord.js';

export default class InviteCommand extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['invite'];
    this.allowDM = true;
    this.category = 'misc';
    this.cooldown = 0;
    this.description = 'utils';
    this.disable = false;
    this.example = '{prefix} invite';
    this.onlyDev = false;
  }

  execute = (): Promise<Message> => this.messageEmbed({
    description: `[${this.translation('INVITE')}](${process.env.ADD_LINK})`,
  });
}
