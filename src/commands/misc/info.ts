import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import {Message} from 'discord.js';
import {arch, cpus, freemem, platform, totalmem} from 'os';
import childProcess from 'child_process';
import * as util from 'util';

export const byteToGigabyte = (byte: number) => {
  return ((byte / 1024) / 1024) / 100;
};

export const parseUptime = (uptime: number): string => {
  console.log(Date.now());
  return (uptime/1000).toString();
};

export default class InfoCommand extends BaseCommand {
  public alias: string[];
  public allowDM: boolean;
  public category: CategoryInterface;
  public cooldown: number;
  public description: string;
  public disable: boolean;
  public example: string;
  public onlyDev: boolean;
  public size: 16|32|64|128|256|512|1024|2048|4096;

  public constructor(data: CommandParams) {
    super(data);

    this.alias = ['info'];
    this.allowDM = true;
    this.category = 'misc';
    this.cooldown = 1;
    this.description = 'utils';
    this.disable = false;
    this.example = '{prefix} info';
    this.onlyDev = false;
  }

  public execute = async (): Promise<Message> => {
    const ram = Math.round(byteToGigabyte(totalmem() - freemem())) / 10;
    const ramHeapTotal = Math.round(byteToGigabyte(totalmem())) / 10;
    const ramPercent = Math.round((ram / ramHeapTotal) * 1000) / 10;

    const exec = util.promisify(childProcess.exec);
    // const {stdout} = await exec('git log --pretty="%ct" -n 1');
    // const {stdout} = await exec('git show -s --date=format:%cd --format=%cd');
    const {stdout} = await exec('git show -s --format=%ct');
    const oneDay = ((24 * 60) * 60) * 1000;

    const cpu = cpus()[0];
    return this.messageEmbed({
      fields: [
        {
          name: this.translation('SERVERS'),
          value: this.client.guilds.cache.size.toString(),
          inline: true,
        },
        {
          name: this.translation('USERS'),
          value: this.client.users.cache.size.toString(),
          inline: true,
        },
        {
          name: this.translation('CHANNELS'),
          value: this.client.channels.cache.size.toString(),
          inline: true,
        },
        {
          name: this.translation('PROCESSOR'),
          value: cpu.model,
          inline: false,
        },
        {
          name: this.translation('PLATFORM'),
          value: platform(),
          inline: true,
        },
        {
          name: this.translation('ARCHITECTURE'),
          value: arch(),
          inline: true,
        },
        {
          name: this.translation('RAM'),
          value: `${ram}Gb / ${ramHeapTotal}Gb (${ramPercent}%)`,
          inline: true,
        },
        {
          name: this.translation('UPTIME'),
          value: parseUptime(this.client.uptime),
          inline: true,
        },
        {
          name: this.translation('LAST_UPDATE'),
          value: `${this.parseUnixTimestamp(parseInt(stdout))} | v.${process.env.npm_package_version}`,
          inline: true,
        },
      ],
    });
  }
}
