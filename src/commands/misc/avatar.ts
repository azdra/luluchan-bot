import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {CommandParams} from '../../types/CommandParams';
import {Message} from 'discord.js';

export default class AvatarCommand extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  private readonly size: 16|32|64|128|256|512|1024|2048|4096;

  constructor(data: CommandParams) {
    super(data);

    this.alias = ['avatar'];
    this.allowDM = true;
    this.category = 'misc';
    this.cooldown = 1;
    this.description = 'Display his avatar or that of the member mentioned';
    this.disable = false;
    this.example = '{prefix} avatar [@member|ID]';
    this.onlyDev = false;

    this.size = 512;
  }

  execute = (): Promise<Message> => {
    if (this.channel.type === 'DM') {
      return this.messageEmbed({
        description: `[Your avatar!](https://cdn.discordapp.com/avatars/${this.author.id}/${this.author.avatar}.png?size=${this.size})`,
        image: {
          url: `https://cdn.discordapp.com/avatars/${this.author.id}/${this.author.avatar}?size=${this.size}`,
        },
      });
    } else {
      return this.messageEmbed({
        description: `[${this.isMe() ? 'Your' : this.member.displayName} avatar!](${this.member.user.displayAvatarURL({size: this.size})})`,
        image: {
          url: this.member.user.displayAvatarURL({size: this.size}),
        },
      });
    }
  };
}
