import {BaseCommand, CategoryInterface} from '../../components/BaseCommand/BaseCommand';
import {Message} from 'discord.js';
import {CommandParams} from '../../types/CommandParams';
import {COMMAND} from '../../app';
import {makeRequest} from '../../utils/makeRequest';
import {ApiCommandInterface} from '../../interfaces/ApiCommandInterface';

type FetchImage = {
  name: string;
  url: string;
}

type Reaction = {
  id: number,
  uses: number,
  author: string,
  member: string,
  command: ApiCommandInterface
}

export default class ReactionCommand extends BaseCommand {
  alias: string[];
  allowDM: boolean;
  category: CategoryInterface;
  cooldown: number;
  description: string;
  disable: boolean;
  example: string;
  onlyDev: boolean;

  // private message: string;
  private multipleCommand: boolean;

  constructor(data: CommandParams) {
    super(data);

    this.alias = COMMAND.get('reaction');
    this.allowDM = false;
    this.category = 'setting';
    this.cooldown = 10;
    this.description = 'jsp';
    this.disable = false;
    this.example = 'jsp';
    this.onlyDev = false;

    // this.message = '';
    this.multipleCommand = true;
  }

  /**
   * @param {string} name
   * @return {Promise<FetchImage>}
   */
  private fetchImage = (name: string): Promise<FetchImage> => makeRequest({
    method: 'GET',
    url: `${process.env.API_URL}/img/${name}`,
  })

  private updateReaction = () => makeRequest({
    method: 'POST',
    url: `${process.env.API_URL}/command-reaction`,
    data: {
      'author': this.author.id,
      'member': this.member.id,
      'command': this.command,
    },
  })

  execute = async (): Promise<Message> => {
    const image = await this.fetchImage(this.command);

    const reaction: Boolean | Reaction = !this.isMe() ? await this.updateReaction() : true;

    const footer = !(reaction instanceof Boolean) ? `${image.name}` : image.name;
    console.log(image);

    return this.messageEmbed({
      image: {
        url: image.url,
      },
      timestamp: new Date(),
      footer: {
        text: footer,
      },
    });
  }
}
