import axios from 'axios';
import RequestInterface from '../interfaces/requestInterface';
import {danger} from './log';

/**
 * makeRequest
 * @param {RequestInterface} config
 * @return {Promise}
 */
export const makeRequest = (config: RequestInterface): Promise<any> => {
  return axios({
    method: config.method,
    url: config.url,
    headers: {
      'Authorization': 'Bearer ' + process.env.API_TOKEN,
    },
    data: config.data,
  })
      .then((res) => res.data)
      .catch((error) => {
        if (error.code === 'ECONNREFUSED' && typeof error.response === 'undefined') {
          danger(`\n/!\\ API SERVER IS NOT STARTED. PLEASE START IT! /!\\`);
          danger(`/!\\ API SERVER IS NOT STARTED. PLEASE START IT! /!\\`);
          danger(`/!\\ API SERVER IS NOT STARTED. PLEASE START IT! /!\\\n`);
          process.exit(1);
        }
        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 404:
              throw new Error(`No route found for this url "${config.url}" for the method "${config.method}"`);
            case 502:
              throw new Error(`Impossible to connect to the API`);
          }
        }
      });
};
