import fs from 'fs';
import path from 'path';
import LoadDirInterface from '../../interfaces/loadDirInterface';
import {danger} from '../log';
import {commandCache} from '../../app';

export const reloadFiles = (config: LoadDirInterface) => {
  fs.readdir(config.dirPath, (err, directory) => {
    if (typeof directory !== 'object') {
      danger('[Lulu-Chan] Impossible to get the pathDir.');
      return process.exit(0);
    }
    directory.forEach((file) => {
      const filePath = path.resolve(config.dirPath, file);
      fs.stat(filePath, async (err, stats) => {
        if (stats.isDirectory()) {
          reloadFiles({dirPath: filePath, client: config.client, handleType: config.handleType});
        } else if (stats.isFile() && file.endsWith('.js')) {
          try {
            delete require.cache[require.resolve(filePath)];

            const {default: _event} = await import(filePath);
            if (_event) {
              if (config.handleType === 'event') {
                _event(config.client);
              } else {
                const {alias} = new _event();
                if (alias) {
                  alias.forEach((elm: string) => {
                    commandCache.set(elm, _event);
                  });
                }
              }
            }
          } catch (e) {
            danger(path.basename(filePath)+' is not configured');
          }
        }
      });
    });
  });
};
