import * as fs from 'fs';
import path from 'path';
import {danger} from '../log';
import LoadDirInterface from '../../interfaces/loadDirInterface';
import {COMMAND, commandCache, loadLen} from '../../app';

/**
 * Load Directory for the command or event
 * @param {LoadDirInterface} config
 */
export const loadFiles = async (config: LoadDirInterface) => {
  await fs.readdir(config.dirPath, (err, directory) => {
    if (typeof directory !== 'object') {
      danger(`Impossible to get this path: ${config.dirPath}.`);
      return process.exit(0);
    }
    directory.forEach((file) => {
      const filePath = path.resolve(config.dirPath, file);
      fs.stat(filePath, async (err, stats) => {
        if (stats.isDirectory()) {
          return await loadFiles({dirPath: filePath, client: config.client, socket: config.socket, handleType: config.handleType});
        } else if (stats.isFile() && (filePath.endsWith('.ts') || filePath.endsWith('.js'))) {
          const {default: _event} = await import(filePath);
          if (_event) {
            try {
              if (!loadLen[config.handleType]) loadLen[config.handleType] = 0;
              loadLen[config.handleType]++;
              if (config.handleType === 'event' || config.handleType === 'socketEvent') {
                return await _event(config.client, config.socket);
              } else if (config.handleType === 'command') {
                const cmd = await new _event({client: null, message: null, prefix: null});
                if (!cmd.multipleCommand) {
                  if (!COMMAND.get(cmd.category)) {
                    COMMAND.set(cmd.category, [cmd.alias[0]]);
                  } else {
                    COMMAND.get(cmd.category).push(cmd.alias[0]);
                  }
                }

                if (cmd.alias) {
                  cmd.alias.forEach((elm: string) => {
                    commandCache.set(elm, _event);
                  });
                }
                return;
              }
            } catch (e) {
              console.log(e);
              danger(path.basename(filePath)+' is not configured');
            }
          }
        }
      });
    });
  });
};
