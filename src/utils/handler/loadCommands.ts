import CommandCacheInterface from '../../interfaces/commandCacheInterface';
import {COMMAND} from '../../app';
import {makeRequest} from '../makeRequest';

/**
 * Load All Command from the DB
 * @return {Map<string, any>}
 */
export const loadCommands = () => {
  return makeRequest({
    method: 'GET',
    url: `${process.env.API_URL}/commands/`,
  }).then((res: Array<CommandCacheInterface>) => {
    if (res.length >= 1) {
      res.forEach((commandType) => {
        for (const commandElement of commandType.command) {
          if (!COMMAND.get(commandType.type)) {
            COMMAND.set(commandType.type, [commandElement.name]);
          } else {
            COMMAND.get(commandType.type).push(commandElement.name);
          }
        }
      });
    }
    return COMMAND;
  }).catch((err) => console.log(err));
};
