/**
 * @param {string} ms
 * @return {string}
 */
export const formatTime = (ms: any): any => {
  const timeMs = new Date(ms);
  const minutes = timeMs.getMinutes();
  const seconds = timeMs.getSeconds();
  const milliseconds = timeMs.getMilliseconds();

  let time = '';
  time += minutes ? '' + minutes : '';
  time += seconds ? '' + seconds : seconds;
  time += '.'+milliseconds;

  return time;
};

export const uptime = (uptime: number): string => {
  const timeSeconds = uptime / 1000;
  let d: number | string = Math.floor(((timeSeconds / 3600) / 24));
  d = d !== 0 ? d >= 10 ? `${d}d` : `0${d}d` : '';

  let h: number | string = Math.floor((timeSeconds / 3600) % 24);
  h = h !== 0 ? h >= 10 ? `${h}h` : `0${h}h` : '';

  let m: number | string = Math.floor((timeSeconds / 60) % 60);
  m = m !== 0 ? m >= 10 ? `${m}m` : `0${m}m` : '';

  let s: number | string = Math.floor(timeSeconds % 60);
  s = s !== 0 ? s >= 10 ? `${s}s` : `0${s}s` : '';

  return `${d} ${h} ${m} ${s}`;
};

/**
 * @param {string} ms
 * @return {string}
 */
export const formatDate = (ms: string): string => {
  return '';
};
