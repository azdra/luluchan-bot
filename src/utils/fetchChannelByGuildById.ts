import LuluChan from '../components/LuluChan/luluchan';

export const fetchChannelByGuildById = async (client: LuluChan, guildID: string) => {
  const guild = await client.guilds.fetch(guildID);
  return guild.channels.cache.filter((channel) => channel.type === 'GUILD_TEXT');
};
