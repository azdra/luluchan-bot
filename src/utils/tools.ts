import './tools';
import {CategoryInterface} from '../components/BaseCommand/BaseCommand';

/**
 * @param {boolean} b
 * @return {string}
 */
export const boolToStr = (b: boolean): string => {
  return b ? 'YES' : 'NO';
};

/**
 * @param {CategoryInterface} category
 * @return {string}
 */
export const getCategory = (category: CategoryInterface) => {
  const categoriesType = {
    'moderator': ':hammer: Moderation',
    'music': ':musical_note: Music',
    'image': ':frame_photo: Image',
    'emote': '<:write:616957647509389324> Emote',
    'reaction': '<:raphi:648949919801016350> Reaction',
    'hentai': ':underage: Nsfw Anime',
    'porn': ':underage: Nsfw',
    'misc': ':file_folder: Misc',
    'setting': ':gear: Setting',
    'admin': ':hammer: Administrator',
  };

  return categoriesType[category];
};

/**
 * @param {string} imageUrl
 * @return {string}
 */
export const getImageId = (imageUrl: string): string => {
  return imageUrl.substring(imageUrl.lastIndexOf('/')+1).replace(/\.[^/.]+$/, '');
};

/**
 * @param {string} s
 * @return {string}
 */
export const toFirstUpper = (s: string): string => {
  const c = s.toLowerCase();
  s = s[0].toUpperCase();
  return s+c.slice(1);
};
