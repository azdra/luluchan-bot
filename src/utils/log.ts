import colors from 'colors';

/**
 * Info Console Log
 * @param {string} content
 * @return {Console}
 */
export const info = (content: string) => {
  return console.log(colors.cyan(`[Lulu-Chan] ${content}`));
};

/**
 * Success Console Log
 * @param {string} content
 * @return {Console}
 */
export const success = (content: string) => {
  return console.log(colors.green(`[Lulu-Chan] ${content}`));
};

/**
 * Danger Console Log
 * @param {string} content
 * @return {Console}
 */
export const danger = (content: string) => {
  return console.log(colors.red(`[Lulu-Chan] ${content}`));
};
