export type Setting = {
  id: string;
  prefix: string;
  color: string;
  lang: string;
  code: string;
}
