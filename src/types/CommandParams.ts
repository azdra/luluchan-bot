import LuluChan from '../components/LuluChan/luluchan';
import {Message} from 'discord.js';
import {Setting} from './Setting';

export type CommandParams = {
  client: LuluChan;
  message: Message;
  prefix: string;
  settings: Setting;
  command: string;
}
