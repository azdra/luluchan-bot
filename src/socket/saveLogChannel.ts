import {Socket} from 'socket.io';
import {HexColorString, MessageEmbed} from 'discord.js';
import color from '../utils/color';
import LuluChan from '../components/LuluChan/luluchan';
import {makeRequest} from '../utils/makeRequest';

const SaveLogChannel = (client: LuluChan, socket: Socket) => {
  socket.on('SAVE:OTHER:LOG:CHANNEL', async (data, callback) => {
    makeRequest({
      method: 'PUT',
      url: `http://localhost:3001/api/v1/settings/${data.guild}/log`,
      data: {
        other: data.channel,
      },
    }).then(async (r) => {
      const channel = await client.channels.fetch(data.channel);
      if (channel && channel.isText()) {
        const embed = new MessageEmbed();
        embed.setDescription('The log channel has been set here via the website!');
        embed.setColor(color.success as HexColorString);
        await channel.send({embeds: [embed]});

        callback('good');
      }
    });
  });
};

export default SaveLogChannel;
