import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';

interface guildUserSocket {
  id: string;
  name: string;
  icon?: string;
  owner: boolean;
  permissions: string;
  features: string[];
  isAdd: boolean;
  banner?: string;
}

const BotGetGuild = (client: LuluChan, socket: Socket) => {
  socket.on('BOT:GET:GUILD', async (guild: guildUserSocket[], callback) => {
    const guildFilter = guild.filter((guild) => (parseInt(guild.permissions) % 32) === 31);
    const guildSort = guildFilter.sort((a, b) => a.name.localeCompare(b.name));
    const isAdd = [];
    const notAdd = [];
    for (const g of guildSort) {
      await client.guilds.fetch(g.id).then((r) => {
        g.isAdd = true;
        g.banner = r.banner;
        isAdd.push(g);
      }).catch(() => {
        g.banner = null;
        g.isAdd = false;
        notAdd.push(g);
      });
    }
    const guilds = isAdd.concat(notAdd);
    callback(guilds);
  });
};

export default BotGetGuild;


