import {MessageEmbed} from 'discord.js';
import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';
import {cacheSettings} from '../app';

const ColorChangeEvent = (client: LuluChan, socket: Socket) => {
  socket.on('SITE:COLOR:CHANGE', async (args) => {
    const cacheGuild = cacheSettings.get(args.guild);
    if (cacheGuild) {
      cacheSettings.get(args.guild).color = args.color;
    }
    const channel = await client.channels.fetch('653610950141935668');
    if (channel.isText()) {
      const user = await client.users.fetch('311874717504110593');
      if (user) {
        const embed = new MessageEmbed();
        embed.setDescription(`${user.toString()} just changed the color to \`${args.color}\` via the web site!`);
        embed.setColor(args.color);

        await channel.send({embeds: [embed]});
      }
    }
  });
};

export default ColorChangeEvent;


