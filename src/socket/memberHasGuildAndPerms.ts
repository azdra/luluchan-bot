import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';

const MemberHasGuildAndPerms = (client: LuluChan, socket: Socket) => {
  socket.on('MEMBER:HAS:GUILD:AND:PERMS', async ({userID, guildID}, callback) => {
    client.guilds.fetch(guildID).then((guild) => {
      guild.members.fetch(userID).then((member) => {
        if (!member.permissions.has('MANAGE_GUILD')) {
          return callback({error: 'memberNotHasManagePermissions'});
        } else {
          return callback({success: true, userID, guildID});
        }
      }).catch((err) => {
        return callback({error: 'memberNotFound'});
      });
    }).catch((err) => {
      return callback({error: 'guildNotFound'});
    });
  });
};

export default MemberHasGuildAndPerms;
