import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';
import {io} from '../app';

const BotAddServer = (client: LuluChan, socket: Socket) => {
  socket.on('BOT:ADD:SERVER', async (args) => {
    // io.to(socket.id).emit('BOT:ADD:SERVER:RECEIVE', {guild: args, added: true});
    client.on('guildCreate', (guild) => {
      if (guild.id === args) {
        io.to(socket.id).emit('BOT:ADD:SERVER:RECEIVE', {guild: args, added: true});
      }
    });
  });
};

export default BotAddServer;


