import {Socket} from 'socket.io';
import {MessageEmbed} from 'discord.js';
import color from '../utils/color';
import LuluChan from '../components/LuluChan/luluchan';
import {cacheSettings} from '../app';

const PrefixChangeEvent = (client: LuluChan, socket: Socket) => {
  socket.on('SITE:PREFIX:CHANGE', async (args) => {
    const cacheGuild = cacheSettings.get(args.guild);
    if (cacheGuild) {
      cacheSettings.get(args.guild).prefix = args.prefix;
    }
    const channel = await client.channels.fetch('653610950141935668');
    if (channel.isText()) {
      const user = await client.users.fetch('311874717504110593');
      if (user) {
        const embed = new MessageEmbed();
        embed.setDescription(`${user.toString()} just changed the prefix to \`${args.prefix}\` via the web site!`);
        embed.setColor(color.success);

        await channel.send({embeds: [embed]});
      }
    }
  });
};

export default PrefixChangeEvent;


