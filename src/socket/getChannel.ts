import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';
import {fetchChannelByGuildById} from '../utils/fetchChannelByGuildById';
import {makeRequest} from '../utils/makeRequest';

const getChannelSocketEvent = (client: LuluChan, socket: Socket) => {
  socket.on('GET:CHANNEL:BY:GUILD:LOG', async (guildId: string, callback) => {
    makeRequest({
      method: 'GET',
      url: `http://localhost:3001/api/v1/settings/${guildId}/log`,
    }).then(async (r) => {
      const channels = await fetchChannelByGuildById(client, guildId);
      callback({
        log: r,
        channels: channels.sort((a, b) => a.name.localeCompare(b.name)),
      });
    });
  });
};

export default getChannelSocketEvent;
