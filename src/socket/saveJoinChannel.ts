import {Socket} from 'socket.io';
import LuluChan from '../components/LuluChan/luluchan';
import {makeRequest} from '../utils/makeRequest';

const SaveJoinChannel = (client: LuluChan, socket: Socket) => {
  socket.on('SAVE:SETTING:JOIN:LOG', async ({guild, enable, channel, message}, callback) => {
    await makeRequest({
      method: 'PUT',
      url: `http://localhost:3001/api/v1/settings/${guild}/log/join`,
      data: {
        enabled: enable,
        channel,
        message,
      },
    });
  });
};

export default SaveJoinChannel;
