import * as dotenv from 'dotenv';
dotenv.config();
import LuluChan from './components/LuluChan/luluchan';
import * as Sentry from '@sentry/node';

type CacheSettingsType = {
  color: string;
  prefix: string;
  lang: string;
  code: string;
}

export const getProjectDir = __dirname;
export const cacheSettings = new Map<string, CacheSettingsType>();
export const commandCache = new Map<string, any>();
// export const musicQueue: PlayList = {};
export const COMMAND = new Map<string, string[]>();
const SOCKET_PORT = process.env.SOCKET_PORT || 3002;
export const loadLen = [];

import {createServer} from 'http';
import {Server} from 'socket.io';
import {Intents} from 'discord.js';
import * as log from './utils/log';
// import {PlayList} from './libs/commands/music/play';

// Init Sentry
Sentry.init({
  dsn: 'https://aac20be0b6e54c9dbbcfec7e2e261794@o956116.ingest.sentry.io/5905467',
  tracesSampleRate: 1.0,
});

// Create Socket Server
const httpServer = createServer();
export const io = new Server(httpServer, {
  cors: {
    methods: ['GET', 'POST'],
    allowedHeaders: ['my-custom-header'],
    credentials: true,
  },
});

// Listen Socket server on port
httpServer.listen(SOCKET_PORT, () => {
  log.success(`Socket.io start on port ${SOCKET_PORT}`);

  // Start Bot
  new LuluChan({
    intents: [
      Intents.FLAGS.GUILDS,
      Intents.FLAGS.GUILD_MESSAGES,
      Intents.FLAGS.DIRECT_MESSAGES,
    ],
    development: true,
    token: process.env.TOKEN,
  });
});
