import {Snowflake} from 'discord.js';
import LuluChan from '../../components/LuluChan/luluchan';
import {cacheSettings, commandCache} from '../../app';
import {getSettings} from '../../components/Settings/getSettings';
import {BaseCommand} from '../../components/BaseCommand/BaseCommand';
import {formatTime} from '../../utils/formatTime';
import color from '../../utils/color';
const cooldown = new Map<string, any>();

/**
 * @param {LuluChan} client
 */
export default function(client: LuluChan) {
  client.on('messageCreate', async (message): Promise<any> => {
    let prefix: string | false = '';
    const owners = process.env.OWNER;

    if (message.author.bot) return;
    if (client.development) {
      if (owners && !owners.includes(message.author.id) || message.channel.id !== '653610950141935668' && message.channel.id !== '685214662572507138') {
        return;
      }
    }

    const clientId: Snowflake | null = client.user ? client.user.id : null;
    const guildId: Snowflake | null = message.guild ? message.guild.id : null;
    const id: string | null = guildId || clientId;
    if (!id) return;
    if (!cacheSettings.get(id)) {
      const data = await getSettings(id);
      cacheSettings.set(id, data.setting);
    }
    const settings = cacheSettings.get(id);

    const prefixes: string[] = [`<@${clientId}>`, `<@!${clientId}>`, 'lulu', settings.prefix];
    prefix = prefixes.find((p) => message.content.toLowerCase().startsWith(p));

    if (!prefix || (message.content.slice(0, prefix.length).toLowerCase() !== prefix.toLowerCase())) return;

    const args: string[] = message.content.slice(prefix.length).trim().split(/ +/g);
    const argsShift: string | undefined = args.shift();
    const command = argsShift ? argsShift.toLowerCase() : null;
    if (!command) return;

    const CommandFile = commandCache.get(command);
    if (!CommandFile) return;

    const CTOR = {client, message, prefix, settings, command};
    const cmd: BaseCommand = new CommandFile(CTOR);

    // CHECK IF THE COMMAND IS ALLOW TO MP
    if ((!cmd.allowDM) && (message.channel.type === 'DM')) {
      return message.channel.send('This command doesn\'t work in private messages!');
    }

    if (!client.development) {
      const userCooldown = message.author.id+cmd.alias[0];
      const commandCooldown = (cmd.cooldown === 0 || !cmd.cooldown ? 1: cmd.cooldown)*1000;

      // CHECK COOLDOWN
      if (cooldown.get(userCooldown)) {
        const cooldownEnd = commandCooldown-(Date.now()-cooldown.get(userCooldown));
        return cmd.reply(cmd.translation('COOLDOWN').replace(/{time}/, formatTime(cooldownEnd)), {
          delete: 3,
        });
      }

      // SET THE COOLDOWN
      cooldown.set(userCooldown, Date.now());

      // DELETE THE COOLDOWN
      setTimeout(() => {
        cooldown.delete(userCooldown);
      }, commandCooldown);
    }

    // Chek if the command is reserved to developers
    if (cmd.onlyDev && !owners.includes(message.author.id)) {
      const embed = cmd.embed({
        description: cmd.translation('COMMAND_DEVELOPERS'),
        color: color.info,
      });
      return message.channel.send({embeds: [embed]});
    }

    return cmd.execute();
  });
}
