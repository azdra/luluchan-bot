import {ActivityType, PresenceStatusData} from 'discord.js';
import LuluChan from '../../components/LuluChan/luluchan';
import {makeRequest} from '../../utils/makeRequest';
import ClientInfoInterface from '../../interfaces/clientInfoInterface';
import {success} from '../../utils/log';

export default (client: LuluChan) => client.on('ready', () => {
  // Create client if isn't exist or get the client
  makeRequest({
    method: 'POST',
    url: `${process.env.API_URL}/client`,
  }).then(async (res: ClientInfoInterface) => {
    const status: PresenceStatusData = res.status as PresenceStatusData;
    const types: ActivityType = res.activityType as ActivityType;
    await client.user.setPresence({
      status: status,
      activities: [{
        name: res.activityName,
        type: types,
      }],
    });
  }).catch(() => process.exit(1));

  success(`Logged in as ${client.user.tag}`);
});
