interface ClientInfoInterface {
  id: string,
  users: string,
  servers: string,
  activityName: string,
  activityType: string,
  status: string
}

export default ClientInfoInterface;
