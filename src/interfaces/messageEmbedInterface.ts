import {
  ColorResolvable,
  EmbedFieldData,
  FileOptions,
  MessageAttachment,
  MessageEmbedAuthor,
  MessageEmbedThumbnail, MessageEmbedVideo,
} from 'discord.js';

interface MessageEmbedInterface {
  title?: string;
  description?: string;
  image?: string;
  autoSend?: boolean;
  footer?: footer;
  delete?: number;
  url?: string;
  timestamp?: Date | number | boolean;
  color?: ColorResolvable;
  fields?: EmbedFieldData[];
  files?: (MessageAttachment | string | FileOptions)[];
  author?: Partial<MessageEmbedAuthor> & { iconUrl?: string; proxyIconUrl?: string };
  thumbnail?: Partial<MessageEmbedThumbnail> & { proxyUrl?: string };
  video?: Partial<MessageEmbedVideo> & { proxyUrl?: string };
  // footer?: Partial<MessageEmbedFooter> & { icon_url?: string; proxy_icon_url?: string };
}

export interface footer {
  text: string,
  iconUrl?: string;
  proxyIconUrl?: string
}

export default MessageEmbedInterface;
