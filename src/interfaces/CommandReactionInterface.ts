import {ApiCommandInterface} from './ApiCommandInterface';

export interface CommandReactionInterface {
  id: number,
  uses: number,
  author: string,
  member: string,
  command: ApiCommandInterface
}
