import LuluChan from '../components/LuluChan/luluchan';
import {Socket} from 'socket.io';

interface LoadDirInterface {
  dirPath: string;
  client: LuluChan;
  socket?: Socket;
  handleType: 'event' | 'command' | 'socketEvent';
}

export default LoadDirInterface;
