import {GuildMember} from 'discord.js';
import ApiErrorInterface from '../apiErrorInterface';

interface WarnAddInterface extends ApiErrorInterface {
  id?: number,
  reason: string,
  createAt?: Date,
  updateAt?: Date,
  moderator: GuildMember,
  user: GuildMember,
  channel?: any;
  case: number;
  delete?: boolean;
}

export default WarnAddInterface;
