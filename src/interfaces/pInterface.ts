import MessageEmbedInterface from './messageEmbedInterface';
import {
  DMChannel,
  Guild,
  GuildMember,
  Message,
  MessageEmbed, MessageOptions,
  NewsChannel,
  TextChannel,
  User,
} from 'discord.js';
import LuluChan from '../components/LuluChan/luluchan';

interface PInterface {
  message: Message;
  args: string[];
  client: LuluChan;
  command: string;
  author: User;
  guild: Guild;
  channel: TextChannel | DMChannel | NewsChannel;
  member: GuildMember;

  getMember(): GuildMember
  getMember(): User
  reply(content: string, options?: {delete: number}): Promise<Message>
  send(content: string, options?: MessageOptions): Promise<Message>
  newReply(content: string): Promise<Message>
  info(config: MessageEmbedInterface): Promise<Message>
  t(keyTranslation: string): string
  embed(config: MessageEmbedInterface): Promise<Message>
  embed(config: MessageEmbedInterface): Promise<MessageEmbed>
}

export default PInterface;
