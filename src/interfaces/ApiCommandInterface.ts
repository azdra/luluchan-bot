export interface ApiCommandInterface {
  id: number,
  name: string,
  uses: number
}
