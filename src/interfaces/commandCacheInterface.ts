import {ApiCommandInterface} from './ApiCommandInterface';

interface CommandCacheInterface {
  id: number,
  type: string,
  command: ApiCommandInterface[]
}

export default CommandCacheInterface;
