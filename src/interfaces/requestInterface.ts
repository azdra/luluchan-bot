interface FetchInterface {
  method: 'GET' | 'POST' | 'PUT';
  url: string;
  data?: object;
}

interface RequestInterface extends FetchInterface {
  httpCode?: number;
  name?: string;
  message?: string;
}

export default RequestInterface;
